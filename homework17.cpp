#include <iostream>
#include <math.h>

class Vector
{
private:
	int x;
	int y;
	int z;
	int a;
	int b;
	int c;

public:
	Vector() : x(0), y(0), z(0), a(0), b(0), c(0)
	{}
	Vector(int x1, int y1, int z1, int a1, int b1, int c1) : x(x1), y(y1), z(z1), a(a1), b(b1), c(c1)
	{}
	void Show()
	{
		std::cout << "\n" << "Vector Length: " << sqrt((a - x) * (a - x) + (b - y) * (b - y) + (c - z) * (c - z)) << "\n";
	}
};


int main()
{
	Vector v(3,11,10,6,4,9);
	
	v.Show();
	
}

